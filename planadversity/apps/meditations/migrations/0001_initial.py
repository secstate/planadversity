# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-24 21:03
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import django_extensions.db.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Meditation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('text', models.TextField(verbose_name='Meditation Text')),
                ('slug', models.IntegerField(verbose_name='Day of the Year')),
                ('date', models.DateField(blank=True, null=True, verbose_name='Date')),
            ],
            options={
                'ordering': ('-modified', '-created'),
                'abstract': False,
                'get_latest_by': 'modified',
            },
        ),
        migrations.CreateModel(
            name='Response',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('created', django_extensions.db.fields.CreationDateTimeField(auto_now_add=True, verbose_name='created')),
                ('modified', django_extensions.db.fields.ModificationDateTimeField(auto_now=True, verbose_name='modified')),
                ('slug', django_extensions.db.fields.ShortUUIDField(blank=True, editable=False, verbose_name='slug')),
                ('initial_response', models.CharField(blank=True, max_length=255, null=True, verbose_name='Initial Response')),
                ('desired_response', models.CharField(blank=True, max_length=255, null=True, verbose_name='Desired Response')),
                ('notes', models.TextField(verbose_name='Notes')),
                ('meditation', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='meditations.Meditation')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'ordering': ('-created',),
            },
        ),
    ]
